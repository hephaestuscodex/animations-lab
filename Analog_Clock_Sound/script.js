window.onload = function () {

    const hourHand = document.querySelector('.hourHand');
    const minuteHand = document.querySelector('.minuteHand');
    const secondHand = document.querySelector('.secondHand');
    const time = document.querySelector('.time');
    const clock = document.querySelector('.clock');
    const audio = document.querySelector('.audio');

    function setDate() {
        const today = new Date();

        const second = today.getSeconds();
        const secondDeg = ((second / 60) * 360) + 360;
        secondHand.style.transform = `rotate(${secondDeg}deg)`;

        audio.play();

        const minute = today.getMinutes();
        const minuteDeg = ((minute / 60) * 360);
        minuteHand.style.transform = `rotate(${minuteDeg}deg)`;

        const hour = today.getHours();
        const hourDeg = ((hour / 12) * 360);
        hourHand.style.transform = `rotate(${hourDeg}deg)`;

        time.innerHTML = '<span>' + '<strong>' + hour + '</strong>' + ' : ' + minute + ' : ' + '<small>' + second + '</small>' + '</span>';

    }

    setInterval(setDate, 1000);
}

function renderTime() {
  
  // Date
  let mydate = new Date();
  let year = mydate.getYear();
  if(year < 1000) {
    year += 1900;
  }
  let day = mydate.getDay();
  let month = mydate.getMonth();
  let daym = mydate.getDate();
  let dayarray = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
  let montharray = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
// Date End

let myClock = document.getElementById("date");
myClock.textContent = " " + dayarray[day] + ", "  + montharray[month] + " " + daym + " " + year;
myClock.innerText = " " + dayarray[day] + ", " + montharray[month] + " " + daym + " " + year;
}

renderTime()
