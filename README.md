Animation projects using JavaScript, CSS, and HTML.

Objectives:
I will be building several animation projects such as clocks and games to showcase my knowledge of JavaScript, HTML, and CSS.

Included in each folder is a screen-shot.png of the project.